use crate::Error;
use crate::Result;

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum Verbose {
  None = 0,
  Low = 1,
  Normal = 2,
  High = 3,
}

impl Verbose {
  pub fn is_quiet(&self) -> bool {
    *self == Verbose::None
  }

  pub fn is_low(&self) -> bool {
    *self == Verbose::Low
  }

  pub fn is_normal(&self) -> bool {
    *self == Verbose::Normal
  }

  pub fn is_high(&self) -> bool {
    *self == Verbose::High
  }

  pub fn is_at_least_quiet(&self) -> bool {
    self.as_u8() >= Verbose::None.as_u8()
  }

  pub fn is_at_least_low(&self) -> bool {
    self.as_u8() >= Verbose::Low.as_u8()
  }

  pub fn is_at_least_normal(&self) -> bool {
    self.as_u8() >= Verbose::Normal.as_u8()
  }

  pub fn is_at_least_high(&self) -> bool {
    self.as_u8() >= Verbose::High.as_u8()
  }

  pub fn from_u8(v: u8) -> Result<Verbose> {
    match v {
      v if (v == Verbose::None as u8) => Ok(Verbose::None),
      v if (v == Verbose::Low as u8) => Ok(Verbose::Low),
      v if (v == Verbose::Normal as u8) => Ok(Verbose::Normal),
      v if (v == Verbose::High as u8) => Ok(Verbose::High),
      v => Err(Error::Unknown(format!("Unknown verbose level '{}'", v))),
    }
  }

  pub fn as_u8(&self) -> u8 {
    *self as u8
  }

  pub fn next(&self) -> Result<Verbose> {
    match self {
      Verbose::None => Ok(Verbose::Low),
      Verbose::Low => Ok(Verbose::Normal),
      Verbose::Normal => Ok(Verbose::High),
      Verbose::High => Err(Error::Unknown(format!(
        "Maximum verbose level reached 'High'"
      ))),
    }
  }
}

impl std::fmt::Display for Verbose {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    write!(
      f,
      "{}",
      match self {
        Verbose::None => "quiet",
        Verbose::Low => "low",
        Verbose::Normal => "normal",
        Verbose::High => "high",
      }
    )
  }
}

impl std::str::FromStr for Verbose {
  type Err = Error;

  fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
    let lo_s = s.to_lowercase();
    match lo_s.as_str() {
      "none" | "quiet" | "0" => Ok(Verbose::None),
      "low" | "1" => Ok(Verbose::None),
      "normal" | "2" => Ok(Verbose::None),
      "high" | "3" => Ok(Verbose::None),
      v => Err(Error::Unknown(format!("Unknown verbose level '{}'", v))),
    }
  }
}
