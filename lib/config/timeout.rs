use std::time::Duration;
use crate::Error;
use crate::Result;

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct ScannerTimeout(pub std::time::Duration);

impl std::str::FromStr for ScannerTimeout {
  type Err = crate::Error;

  fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
    lazy_static! {
      static ref r: regex::Regex = regex::Regex::new(r"^([\d]+)(ns|us|ms|s)$").unwrap();
    }
    if let Some(caps) = r.captures(s) {
      let dur = caps.get(1).unwrap().as_str();
      let unit = caps.get(2).unwrap().as_str();
      Ok(match unit {
        "ns" => ScannerTimeout(Duration::from_nanos(dur.parse::<u64>().or_else(|e| {
          Err(Error::Unknown(format!("Invalid nanosecond duration '{}', {}", dur, e.to_string())))
        })?)),
        "us" => ScannerTimeout(Duration::from_micros(dur.parse::<u64>().or_else(|e| {
          Err(Error::Unknown(format!("Invalid microsecond duration '{}', {}", dur, e.to_string())))
        })?)),
        "ms" => ScannerTimeout(Duration::from_millis(dur.parse::<u64>().or_else(|e| {
          Err(Error::Unknown(format!("Invalid millisecond duration '{}', {}", dur, e.to_string())))
        })?)),
        "s" => ScannerTimeout(Duration::from_secs(dur.parse::<u64>().or_else(|e| {
          Err(Error::Unknown(format!("Invalid second duration '{}', {}", dur, e.to_string())))
        })?)),
        _ => {
          return Err(Error::Unknown(format!(
              "Invalid duration unit '{}' to parse '{}', valid ones are: ns, us, ms, s",
              unit, dur
            )))
        }
      })
    } else {
      return Err(Error::Unknown(format!("Invalid timeout, format is: {}", r"^([\d\.]+)(ns|us|ms|s)$")));
    }
  }
}