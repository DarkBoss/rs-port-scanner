use crate::Error;
use crate::Scanner;
use std::net::IpAddr;
use crate::Result;

#[derive(Debug, Clone, Eq, PartialEq)]
pub enum ScannerHosts {
  None,
  LAN,
  Some(Vec<String>)
}

impl ScannerHosts {
  pub fn build(&self) -> Result<Vec<IpAddr>> {
    match self {
      ScannerHosts::None => Ok(vec![]),
      ScannerHosts::LAN => Ok(Scanner::build_host_list(vec![
        "192.168.1.1/32",
        "192.168.1.2/31",
        "192.168.1.4/30",
        "192.168.1.8/29",
        "192.168.1.16/28",
        "192.168.1.32/27",
        "192.168.1.64/26",
        "192.168.1.128/25"
      ].into_iter())?),
      ScannerHosts::Some(list) => {
        match list.len() {
          0 => Err(Error::Unknown("No hosts given".to_string())),
          _n => Ok(Scanner::build_host_list(list.into_iter())?),
        }
      }
    }
  }
}
