extern crate clap;

use crate::Result;
use crate::Error;
use crate::ScannerConfig;
use crate::Verbose;
use crate::ScannerTimeout;
use crate::ScannerPorts;
use crate::ScannerHosts;

use clap::{App, Arg};

pub fn parse_options<'a>() -> Result<ScannerConfig> {
  let args: clap::ArgMatches<'a> = App::new(env!("CARGO_PKG_NAME"))
    .author(env!("CARGO_PKG_AUTHORS"))
    .version(env!("CARGO_PKG_VERSION"))
    .arg(Arg::with_name("verbose").long("verbose").short("v").multiple(true).help("show additional log messages"))
    .arg(
      Arg::with_name("num_jobs")
        .short("j")
        .long("jobs")
        .help("define the number of concurrent threads")
        .default_value("1"),
    )
    .arg(
      Arg::with_name("timeout")
        .short("t")
        .long("timeout")
        .help("define the timeout for port connection")
        .default_value("10ms"),
    )
    .arg(
      Arg::with_name("ports")
        .short("p")
        .long("port")
        .long("ports")
        .help("define the port ranges to be checked")
        .default_value("smart")
        .help(
          "define the port range to be scanned, recognized formats: '0..13', '22,80,...', '22' or 'smart'",
        ),
    )
    .arg(Arg::with_name("HOSTS").multiple(true).required(true).help("define the hosts to be scanned (CIDR), recognized formats: 192.168.2.1, 192.168.1.128/25, ..."))
    .get_matches();

  let mut cfg = ScannerConfig::default();
  cfg.hosts = ScannerHosts::Some(
    args
      .values_of("HOSTS")
      .unwrap()
      .map(|s| s.to_string())
      .collect(),
  );
  if args.is_present("num_jobs") {
    cfg.num_jobs = args
      .value_of("num_jobs")
      .unwrap()
      .parse()
      .expect("invalid number of jobs given");
  }
  
  if let Some(ports) = args.values_of("ports") {
    use std::convert::TryFrom;
    cfg.ports = ScannerPorts::try_from(ports.map(|v| v).collect::<Vec<&str>>()).or_else(|e| {
      Err(Error::Unknown(format!(
        "invalid port number: {}",
        e.to_string()
      )))
    })?;
  }

  if args.is_present("verbose") {
    cfg.verbose = Verbose::from_u8(args.occurrences_of("verbose") as u8)?;
  }

  if let Some(timeout) = args.value_of("timeout") {
    cfg.timeout = timeout.parse()?;
  }
  Ok(cfg)
}
