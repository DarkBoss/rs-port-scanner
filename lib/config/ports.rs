#[derive(Debug, Clone, Eq, PartialEq)]
pub enum ScannerPorts {
  None,
  All,
  Some(Vec<u16>),
  Smart,
}

impl ScannerPorts {
  pub fn build(&self) -> Vec<u16> {
    match self {
      ScannerPorts::None => vec![],
      ScannerPorts::All => (0..std::u16::MAX).map(|p| p).collect::<Vec<u16>>(),
      ScannerPorts::Some(list) => list.clone(),
      ScannerPorts::Smart => vec![22, 25, 80, 8080, 4200, 443, 469, 8000],
    }
  }
}

impl std::convert::TryFrom<Vec<&str>> for ScannerPorts {
  type Error = crate::Error;

  fn try_from(ports: Vec<&str>) -> Result<Self, Self::Error> {
    let mut final_ports: Vec<u16> = vec![];
    let mut smart = false;
    for port in ports {
      let split_ports = port.split(",");
      for split_port in split_ports {
        match split_port {
          "smart" => smart = true,
          "" => Err(crate::Error::Unknown(
            "Invalid port number <empty>".to_string(),
          ))?,
          n => {
            lazy_static! {
              static ref r: regex::Regex = regex::Regex::new(r"([\d]+)\.\.([\d]+)").unwrap();
            }
            if let Some(caps) = r.captures(n) {
              let start = caps.get(1).unwrap().as_str();
              let end = caps.get(2).unwrap().as_str();
              let mut u_start = start.parse::<u16>().or_else(|e| {
                Err(crate::Error::Unknown(format!(
                  "invalid port number '{}', {}",
                  start,
                  e.to_string()
                )))
              })?;
              let mut u_end = end.parse::<u16>().or_else(|e| {
                Err(crate::Error::Unknown(format!(
                  "invalid port number '{}', {}",
                  end,
                  e.to_string()
                )))
              })?;
              if u_start > u_end {
                let u_tmp: u16;
                u_tmp = u_end;
                u_end = u_start;
                u_start = u_tmp;
              }
              for u in u_start..u_end {
                final_ports.push(u);
              }
            } else {
              final_ports.push(
                n.parse::<u16>()
                  .or_else(|e| Err(crate::Error::Unknown(e.to_string())))?,
              );
            }
          }
        }
      }
    }

    if smart && !final_ports.is_empty() {
      return Err(crate::Error::Unknown(
        "'smart' port type must be the only port given".to_string(),
      ));
    }
    if smart {
      Ok(ScannerPorts::Smart)
    } else {
      Ok(ScannerPorts::Some(final_ports))
    }
  }
}

impl std::str::FromStr for ScannerPorts {
  type Err = crate::Error;

  fn from_str(s: &str) -> Result<Self, Self::Err> {
    use std::convert::TryFrom;
    ScannerPorts::try_from(s.split(",").collect::<Vec<&str>>()).or_else(|e| {
      Err(crate::Error::Unknown(format!(
        "failed to convert parse ports, {}",
        e.to_string()
      )))
    })
  }
}
