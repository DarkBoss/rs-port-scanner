pub mod ports;
pub mod hosts;
pub mod verbose;
pub mod timeout;

#[cfg(feature = "app")]
pub mod cli;

pub use ports::*;
pub use hosts::*;
pub use verbose::*;
pub use timeout::*;
#[cfg(feature = "app")]
pub use cli::*;

use std::time::Duration;

/**
 * ScannerConfig represent the configuration for the scanner
 */
#[derive(Debug, Clone, Eq, PartialEq)]
pub struct ScannerConfig {
  pub num_jobs: u8,
  pub hosts: ScannerHosts,
  pub ports: ScannerPorts,
  pub timeout: ScannerTimeout,
  pub verbose: Verbose
}

impl Default for ScannerConfig {
  fn default() -> Self {
    Self {
      num_jobs: 1,
      hosts: ScannerHosts::None,
      ports: ScannerPorts::Smart,
      timeout: ScannerTimeout(Duration::from_millis(10)),
      verbose: Verbose::Low,
    }
  }
}

impl ScannerConfig {
}