use crate::DiscoveredHostList;
use crate::Error;

#[cfg(feature = "threads")]
use threadpool::ThreadPool;

use crate::Result;
use crate::ScannerConfig;
use crate::ScannerHosts;
use crate::ScannerPorts;
use ipnet::IpNet;
use std::net::IpAddr;
use std::net::TcpStream;
use std::time::Duration;
use std::time::Instant;

// use std::sync::mpsc::channel;
// use std::thread;

/// ScanResult represent the result from a scan() call
pub type ScanResult<'a> = DiscoveredHostList<'a>;

/// Scanner allows easy port scanning by aggregating input and output.
/// It scans all ranges, with ports defined and returns a ScanResult.
pub struct Scanner {
  hosts: Vec<IpAddr>,
  ports: Vec<u16>,
  config: ScannerConfig,
}

/// Constructs an empty scanner with a default configuration
impl Default for Scanner {
  fn default() -> Self {
    Self {
      hosts: vec![],
      ports: vec![],
      config: ScannerConfig::default(),
    }
  }
}

/// Scanner implementation
impl Scanner {
  /// Construct a Scanner from a pre-existing configuration
  pub fn from_config(config: ScannerConfig) -> Scanner {
    Self {
      hosts: vec![],
      ports: vec![],
      config,
    }
  }

  /// Retrieve the contained list of hosts
  pub fn hosts(&self) -> &Vec<IpAddr> {
    &self.hosts
  }
  /// Retrieve the contained list of hosts as mutable
  pub fn hosts_mut(&mut self) -> &mut Vec<IpAddr> {
    &mut self.hosts
  }

  /// Retrieve the contained configuration
  pub fn config(&self) -> &ScannerConfig {
    &self.config
  }
  /// Retrieve the contained configuration as mutable
  pub fn config_mut(&mut self) -> &mut ScannerConfig {
    &mut self.config
  }

  /// Scan all hosts with configured ports. Return the result as a list of DiscoveredHost
  pub fn scan(&mut self) -> Result<ScanResult<'_>> {
    use std::io::Write;
    let res: ScanResult = vec![];

    // scan hosts
    let mut prct: f32 = 0.0;
    if self.config.hosts == ScannerHosts::None {
      return Err(Error::InvalidConfiguration {
        msg: "No hosts to scan".to_string(),
        cfg: self.config.clone(),
      });
    }
    if self.config.ports == ScannerPorts::None {
      return Err(Error::InvalidConfiguration {
        msg: "No ports to scan".to_string(),
        cfg: self.config.clone(),
      });
    }
    self.hosts = self.config.hosts.build()?;
    self.ports = self.config.ports.build();
    println!(
      "* scanning {} hosts, {} ports each",
      self.hosts.len(),
      self.ports.len()
    );

    #[cfg(feature = "threads")]
    self.scan_mt()?;
    #[cfg(not(feature = "threads"))]
    self.scan_st()?;
    Ok(res)
  }

  #[cfg(not(feature = "threads"))]
  fn print_scan_percentage(
    host: &IpAddr,
    port: Option<u16>,
    prct: f32,
    start_time: &Instant,
  ) -> std::io::Result<()> {
    use std::io::Write;
    use std::ops::Sub;
    print!(
      "\r\x1b[2K* scanning {}{} ({}%) - {:?}",
      host.to_string(),
      match port {
        Some(p) => format!(":{}", p),
        None => String::new(),
      },
      prct,
      Instant::now().sub(*start_time)
    );
    std::io::stdout().flush()
  }

  pub fn check_port(addr: &IpAddr, port: u16, timeout: Option<Duration>) -> Result<Duration> {
    use std::ops::Sub;
    let start = Instant::now();
    let connect = || match timeout {
      Some(d) => TcpStream::connect_timeout(&std::net::SocketAddr::from((addr.clone(), port)), d),
      None => TcpStream::connect((addr.clone(), port)),
    };
    match connect() {
      Ok(_) => Ok(Instant::now().sub(start)),
      Err(e) => Err(Error::Unknown(e.to_string())),
    }
  }

  pub fn build_port_list() -> Vec<u16> {
    (0..std::u16::MAX).map(|p: u16| p).collect()
  }

  pub fn build_host_list<S, I>(ranges_: I) -> Result<Vec<IpAddr>>
  where
    S: AsRef<str>,
    I: std::iter::Iterator<Item = S>,
  {
    let mut parsed: Vec<Result<Vec<IpAddr>>> = ranges_
      .map(|s| -> Result<Vec<IpAddr>> {
        // parse CIDR notation
        if s.as_ref().find("/").is_none() {
          // parse single ip
          Ok(vec![s.as_ref().parse::<IpAddr>().or_else(|e| {
            Err(Error::Unknown(format!(
              "Invalid host '{}', {}",
              s.as_ref(),
              e.to_string()
            )))
          })?])
        } else {
          // aggregate network hosts
          let net: IpNet = s.as_ref().parse::<IpNet>().or_else(|e| {
            Err(Error::Unknown(format!(
              "Invalid host '{}', {}",
              s.as_ref(),
              e.to_string()
            )))
          })?;
          Ok(net.hosts().collect())
        }
      })
      .collect();
    let mut ret: Vec<IpAddr> = vec![];
    for p in parsed {
      match p {
        Ok(p) => ret.splice(0..0, p),
        Err(e) => return Err(e),
      };
    }
    Ok(ret)
  }

  #[cfg(feature = "threads")]
  fn scan_mt(&self) -> Result<()> {
    let mut pool = ThreadPool::new_with_name("scanner".to_string(), self.config.num_jobs as usize);
    let mut i = 0;
    for host in self.hosts.clone() {
      for port in self.ports.clone() {
        pool.execute(move || Self::scan_mt_port(std::sync::Arc::new((host, port))));
      }
    }
    pool.join();
    Ok(())
  }

  #[cfg(feature = "threads")]
  fn scan_mt_port(data: std::sync::Arc<(IpAddr, u16)>) {
    println!("[{}] {}:{}", std::thread::current().name().unwrap(), data.0, data.1);
    match Self::check_port(&data.0, data.1, Some(Duration::from_millis(10))) {
      Ok(dur) => {}
      Err(_) => {}
    }
  }

  #[cfg(not(feature = "threads"))]
  fn scan_st(&self) -> Result<()> {
    use std::io::Write;
    let mut prct: f32 = 0.0;
    for (host_id, host) in self.hosts.iter().enumerate() {
      // scan host
      let start_time = std::time::Instant::now();
      prct = ((host_id + 1) as f32 / self.hosts.len() as f32) * 100 as f32;
      for (port_id, port) in self.ports.iter().enumerate() {
        if port_id == 0 || (port_id % 10) == 0 {
          match Self::print_scan_percentage(&host, Some(*port), prct, &start_time) {
            Ok(dur) => {}
            Err(_) => {}
          }
        }
        match Self::check_port(host, *port, Some(Duration::from_millis(10))) {
          Ok(dur) => {}
          Err(_) => {}
        }
      }
      match Self::print_scan_percentage(&host, None, prct, &start_time) {
        Ok(dur) => {}
        Err(_) => {}
      }
      std::io::stdout().flush().expect("failed to flush stdout");
      println!("")
    }
    Ok(())
  }
}
