pub struct DiscoveredService<'a> {
  port: u16,
  name: &'a str,
}

impl<'a> DiscoveredService<'a> {
  pub fn new(port: u16, name: &'a str) -> DiscoveredService<'a> {
    DiscoveredService { port, name }
  }

  pub fn port(&self) -> u16 {
    self.port
  }

  pub fn service(&self) -> &'a str {
    self.name
  }
}

impl<'a> std::fmt::Display for DiscoveredService<'a> {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    write!(f, "{}", self.name)
  }
}

pub type DiscoveredServiceList<'a> = Vec<DiscoveredService<'a>>;
