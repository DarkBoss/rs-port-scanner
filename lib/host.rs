use crate::DiscoveredServiceList;
use std::net::IpAddr;

pub struct DiscoveredHost<'a> {
  addr: &'a IpAddr,
  services: DiscoveredServiceList<'a>,
}

impl<'a> DiscoveredHost<'a> {
  pub fn new(addr: &'a IpAddr, services: DiscoveredServiceList<'a>) -> DiscoveredHost<'a> {
    DiscoveredHost { addr, services }
  }

  pub fn addr(&'a self) -> &'a IpAddr {
    &self.addr
  }

  // pub fn addr_mut(&'a mut self) -> &'a mut IpAddr {
  //   &mut self.addr
  // }

  pub fn services(&'a self) -> &'a DiscoveredServiceList<'a> {
    &self.services
  }

  pub fn services_mut(&'a mut self) -> &'a mut DiscoveredServiceList<'a> {
    &mut self.services
  }
}

pub type DiscoveredHostList<'a> = Vec<DiscoveredHost<'a>>;
