use crate::error::Error;
use std::result::Result as BaseResult;

pub type Result<Success> = BaseResult<Success, Error>;