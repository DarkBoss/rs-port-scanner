use std::io::Write;
use crate::Result;
use crate::Error;

pub fn banner<W: Write>(mut w: W) -> Result<()> {
  writeln!(w, "netscan v{} by {}", env!("CARGO_PKG_VERSION"), env!("CARGO_PKG_AUTHORS"))
    .or_else(|e| Err(Error::Unknown(e.to_string())))?;
  Ok(())
}