use crate::ScannerConfig;

#[derive(Debug)]
pub enum Error {
  Unknown(String),
  InvalidConfiguration{msg: String, cfg: ScannerConfig},
  PortClosed(u16),
}


impl std::fmt::Display for Error {
  fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
      write!(f, "{}", match self {
        Error::Unknown(msg) => format!("{}", msg),
        Error::InvalidConfiguration{msg, cfg} => format!("invalid configuration, {} (config: {:#?})", msg, cfg),
        Error::PortClosed(port) => format!("port {} is closed", port),
      })
  }
}