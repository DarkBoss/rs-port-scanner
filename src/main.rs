use netscan::Scanner;

extern crate netscan;

fn main() {
  // parse command line options
  let cfg = match netscan::parse_options() {
    Ok(cfg) => cfg,
    Err(e) => {
      eprintln!("Failed to parse options, {}", e.to_string());
      std::process::exit(42);
    }
  };

  // print banner
  netscan::display::banner(std::io::stdout()).expect("failed to print banner");

  // create scanner config
  let mut scanner = Scanner::from_config(cfg);

  // display scanner config for normal and high verbose levels
  if scanner.config().verbose.is_high() {
    println!("* configuration: {:#?}", scanner.config());
  } else if scanner.config().verbose.is_at_least_normal() {
    println!("* configuration: {:?}", scanner.config());
  }

  // start scanner
  match scanner.scan() {
    Ok(res) => {}
    Err(e) => {
      eprintln!("{}", e.to_string());
      std::process::exit(42);
    }
  };
}
