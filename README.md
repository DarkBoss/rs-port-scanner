# NetScan

Network scanner written in rust.

## Build

This project uses cargo.

```shell
cargo build --features=app,threads
```

## Features

This project uses several togglable features:

| Feature | Description                       |
| ------- | --------------------------------- |
| app     | Enables cli module, and build app |
| threads | Activates multi-threading         |

## Examples

```shell
netscan 192.168.1.0/24 # discover everything on your LAN network.
```